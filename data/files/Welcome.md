# The first Flutter DevMeeting

![Flutter logo](https://devmeeting.wrbl606.ct8.pl/flutter-logo.png)

> TODO: GIF with application interaction

## Goal
We'll be building a recipe app. The app enables the user to store and modify recipies.

The app is a browser for saved recipes. Each of them will be display in detail after clicking on it from the main list. There, the recipe can be edited on the spot, without going to any other screen.

Recipies could be created on the recipe list screen. Deleting the recipe can be done by swiping the recipe on the list or by tapping the 'Trash' button on the details screen.

Each recipe have:
* an optional list of the prepared dish,
* a title,
* an optional description,
* a time-consumption mark,
* an ingredient list,
* a list of steps to prepare a dish,
* a list of predefined kitchen appliences that are required to prepare the dish.


## What you will learn
A list of acquired skills, if all steps will be performed:
1. Understanding basic Flutter capabilities.
2. Ability to create a new Flutter project for Android and iOS.
3. Familiarity of the Flutter CLI.
4. Proper configuration of Android Studio and VS Code IDEs for comfortable and quick Flutter app development with stateful hot reloads.
5. Basic Dart programming langugage knowledge.
6. Usage of basic Flutter widgets.
7. Familiarity of the Dart's package manager, pub.dev.
8. Building "infinite" scrolling lists with a StreamBuilder widget.
9. Differentiation between the stateless and stateful widgets.
10. Creating custom Flutter icon packs based on fonts.
11. Handling app navigation with parameters.
12. Creating interactive layouts that are touchable and editable.
13. Using the widget's state to modify the view.
14. Integrating custom fonts to the Flutter app.
15. Usage and live replacing of the app's theme.
16. Implementation of the InheritedWidget design pattern.
17. Handling of advanced gestures.


## Prerequisites
Complete this steps:
1. Install the Flutter CLI and configure your system with this guide: https://flutter.dev/docs/get-started/install. Steps 1-3 will be sufficient. Then,
2. Run the `flutter channel stable` command in your terminal. Expected result:
```
Switching to flutter channel 'stable'...
git: Your branch is up to date with 'origin/stable'.
git: Already on 'stable'
```
3. Run the `flutter doctor`. Expected result:
```
Doctor summary (to see all details, run flutter doctor -v):
[√] Flutter (Channel stable, v1.12.13+hotfix.8, on Your OS)
[√] Android toolchain - develop for Android devices (Android SDK version 29.0.3)
[√] Android Studio (version 3.6)
```
4. Run the `flutter create testapp`, `cd testapp` and `flutter build apk` commands in your terminal. Expected result:
 ```√ Built build\app\outputs\apk\release\app-release.apk```
5. Open up a emulator/simulator or connect your Android/iOS device to the development machine and run `flutter run` command. The app should be opened on the target device with the Debug mode. You'll recognize the Debug mode by the red ribbon at the top right corner of the app.
6. If everything is as expected, you are ready to go. If not, the CLI probably will show you what to do to fix it. Otherwise you can contact the mentor for help :)

> We'll be using the Android Studio thourghout the course, but feel free to use the VS Code with Flutter extension.


### Ready to start?
[Start the course](#course/1._Create_the_project.md)
